# Elsavie API



# Authentication
Authentication can be done either via API key or token

## API key
Add API key to x-api-key header field and include the header in every request

    x-api-key: {api-key}

## Token
Add token to header Authorization field and include the header in every request
Token is usable only for specific kit defined in request

    Authorization: Bearer {token}

Get the token:

    curl --request POST \
        -d {"code": 111111, "pin": 0000}  \
        --url {url_path}/api/v1/auth/



# API


# Get specific kit
Pull specific kit
Returns list with ONE kit inside

    curl --request GET \
        --url {url_path}/api/v1/kit/{kit_code} \
        --header 'Authorization: Bearer {token}'


    "result": [{
        "code": "number",
        "distributor": "string",
        "locale": "string",
        "status": {
            "name": "string",
            "timeStamp": "string",
            "comment": "string"
        },
        "status_transitions":? [{
            "name": "string",
            "timeStamp": "string",
            "comment": "string"
        }, {}, ...],
        "timeStamp": "string",
        "result"?: {
            "timeStamp": "string",
            "pdf": "string",
            "json": "string",
            "comment": "string"
        },
        "type": "number",
        "comment": "string"
    }]



# Get list of kits
Pull all partner related kits
Returns list of multiple kits

    curl --request GET \
        --url {url_path}/api/v1/kit \
        --header 'Authorization: Bearer {token}'

    "result": [
        ...,
        "code": "number",
        "pin": "number",
        "distributor": "string",
        "locale": "string",
        "status": {
            "name": "string",
            "timeStamp": "string",
            "comment": "string"
        },
        "status_transitions":? [{
            "name": "string",
            "timeStamp": "string",
            "comment": "string"
        }, {}, ...],
        "timeStamp": "string",
        "result"?: {
            "timeStamp": "string",
            "pdf": "string",
            "json": "string",
            "comment": "string"
        },
        "type": "number",
        "comment": "string"
        },
     ... ]




# Get specific questionnaire
Get list of required questions for kit
Posted answers are included, if any

    curl --request GET \
        --url {url_path}/api/v1/qa/{kit_code} \
        --header 'Authorization: Bearer {token}'

    "result": [
        ...,
        {
            "version": "string",
            "questions": [{}],
            "locales": ["string"],
            "result": {},
            "status": [{}]
        },
        ...
    ]


# POST questionnaire  
Post client answers
Returns list of updated ID's or failed items

    curl --request POST \
        -d {"id": "1", "value": 2}  \
        --url {url_path}/api/v1/qa/{kit_code} \
        --header 'Authorization: Bearer {token}'

    "result": {
        "updated": [..., id, id, id, ....],
            ...,
            {
                "id": "number",
                "message": "string"
            },
            ...
        ]
    }


# Change Kit language
Locale must be suported in partner config.

    curl --request POST \
        --url '{url_path}/api/v1/update/{kit_code}' \
        --header 'Content-Type: application/json' \
        --header 'x-api-key: {key}' \
        --data '{
            "locale": "en"
        }'


    {
        "msg": "done"
    }


# Filters

    work in progress
